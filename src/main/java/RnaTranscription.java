import java.util.*;

class RnaTranscription {

    String transcribe(String dnaStrand) {

        if (dnaStrand.length() == 0){
            return "";
        }

        List<String> dnaNucleotides = new ArrayList<String>(Arrays.asList(dnaStrand.split("")));
        int dnaLength = dnaNucleotides.size();
        String rna = "";
        HashMap<String, String> dnaToRna = new HashMap<>();

        dnaToRna.put("A", "U");
        dnaToRna.put("T", "A");
        dnaToRna.put("G", "C");
        dnaToRna.put("C", "G");

          for (int i = 0; i < dnaLength; i++) {
            rna = rna.concat(dnaToRna.get(dnaNucleotides.get(i)));
        }

        return rna;
    }

    public static void main(String[] args) {

        RnaTranscription rnaTranscription = new RnaTranscription();

        Scanner in = new Scanner(System.in);

        System.out.println("Enter the DNA string:");
        String ans = rnaTranscription.transcribe(in.next());
        System.out.println("RNA: "+ ans);

    }

}
